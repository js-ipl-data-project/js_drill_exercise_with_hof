//2. Get all items containing only Vitamin C.

function getOnlyVitaminC(items) {
  if(Array.isArray(items)){
  let vitaminC = items.filter((item) => {
    return item.contains == "Vitamin C";
  });
  return vitaminC;
}else{
  console.log("something went wrong")
}
}

module.exports = getOnlyVitaminC;
