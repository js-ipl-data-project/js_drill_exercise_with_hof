//  5. Sort items based on number of Vitamins they contain.

function sortByVitaminCount(a, b) {
  if (Array.isArray(a) && Array.isArray(b)) {
  const vitaminsA = a.contains.split(", ").length;
  const vitaminsB = b.contains.split(", ").length;
  return vitaminsA - vitaminsB;
  }else{
    console.log("Something went wrong")
  }
}

module.exports = sortByVitaminCount;
