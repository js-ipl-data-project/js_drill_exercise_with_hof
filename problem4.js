// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

function groupOfVitamin(items) {
  if(Array.isArray(items)){
  return items.reduce((groupedVitamin, item) => {
    let vitamins = item.contains.split(", ");
    vitamins.forEach((vitamin) => {
      if (!groupedVitamin[vitamin]) {
        groupedVitamin[vitamin] = [];
      }
      groupedVitamin[vitamin].push(item.name);
    });
    return groupedVitamin;
  }, {});
}else{
  console.log("some thing went wrong")
}
}

module.exports = groupOfVitamin;
